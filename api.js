
(function(Marionette, Backbone, _, $){
    'use strict';


    var EZService = window.EZService || {};
    window.EZService = EZService;

    /* add api point here */
    var EndPoint = Marionette.Object.extend({
        initialize:function(options){
            this.name = options.name;
            this.path = options.path;
            this.silent = options.silent;
            this.extraData = options.extraData || function(){
                return {};
            };
            this.callBack = options.callBack || function(){ };
        }
    });


    /*reference

        https://bitbucket.org/frankchang/ezchannel/wiki/Backend_API#markdown-header-api-collection-ezchannelcollectioncreate-20141127-1530

    */
    var endPoints = [
        new EndPoint({
            name:'login',
            path:'/auth/login',
            callBack: function(api, result){
                api.setToken(result.token);
            }
        }),
        // Public
        new EndPoint({ // ??? Not Available // search is also not available
            name: 'publicChannel',
            path:'/channel/public'
        }),
        // Public the same with above
        new EndPoint({
            name: 'public',
            path:'/channel/public'
        }),
        // Permission Set up
        new EndPoint({
            name: 'permissionPublic',
            path:'/channel/collection/permission/set/public'
        }),
        new EndPoint({
            name: 'permissionFriend',
            path:'/channel/collection/permission/set/friend'
        }),
        new EndPoint({
            name: 'permissionPrivate',
            path:'/channel/collection/permission/set/private'
        }),
        // logout
        new EndPoint({
            name: 'logout',
            path:'/auth/logout'
        }),
        new EndPoint({
            name: 'myChannel',
            path:'/channel/mime'
        }),
        // Subscription Related
        new EndPoint({
            name: 'subscribed',
            path:'/channel/subscribed'
        }),
        new EndPoint({
            name: 'subscribe',
            path:'/channel/subscribe'
        }),
        new EndPoint({
            name: 'unsubscribe',
            path:'/channel/unsubscribe'
        }),
        // Featured
        new EndPoint({
            name: 'featured',
            path:'/channel/featured'
        }),
        // My Channel
        new EndPoint({
            name: 'mime',
            path:'/channel/mime'
        }),
        new EndPoint({
            name: 'mimeAdd',
            path:'/channel/mime/add'
        }),
        // My Channel CID
        new EndPoint({
            name: 'mimeCid',
            path:'/channel/mime/status',
            silent: true
        }),
        // movie crud
        new EndPoint({
            name: 'movieCreate',
            path:'/channel/movie/create'
        }),
        new EndPoint({
            name: 'movieLink',
            path:'/channel/movie/link'
        }),
        new EndPoint({
            name: 'movieOrder',
            path:'/channel/movie/order'
        }),
        // collection crud
        new EndPoint({
            name: 'published',
            path:'/channel/collection/published'
        }),
        new EndPoint({
            name: 'collection',
            path:'/channel/collection/create'
        }),
        new EndPoint({
            name: 'repeatSet',
            path:'/channel/collection/repeat/set'
        }),
        new EndPoint({
            name: 'repeatUnset',
            path:'/channel/collection/repeat/unset'
        }),
        new EndPoint({
            name: 'audioSet',
            path:'/channel/collection/is_audio/set'
        }),
        new EndPoint({
            name: 'audioUnset',
            path:'/channel/collection/is_audio/unset'
        }),
        new EndPoint({
            name: 'createCollection',
            path:'/channel/collection/create'
        }),
        new EndPoint({
            name: 'friend',
            path:'/channel/friend'
        }),
        new EndPoint({
            name: 'collectionRename',
            path:'/channel/collection/rename'
        }),
        new EndPoint({
            name: 'collectionDelete',
            path:'/channel/collection/delete'
        }),
        new EndPoint({
            name: 'movieUnlink',
            path:'/channel/movie/unlink'
        }),
        new EndPoint({
            name: 'descriptionSet',
            path:'/channel/collection/description/set'
        }),
        new EndPoint({
            name: 'movieDelete',
            path:'/channel/movie/delete'
        }),
        new EndPoint({
            name: 'movies',
            path:'/channel/movie'
        }),
        new EndPoint({
            name: 'movieMove',
            path:'/channel/movie/move'
        }),
        new EndPoint({
            name: 'movieCopy',
            path:'/channel/movie/copy'
        }),
        new EndPoint({
            name: 'moviePlaylist',
            path:'/channel/movie/playlist'
        }),
        new EndPoint({
            name: 'listCollection',
            path:'/channel/collection',
            callBack: function(api, result){
                _.each(result.data, function(collection){
                    _.each(collection.videos, function(movie){
                        movie.cid =  collection.id;
                    });
                });
            }
        }),
        // Device Related
        new EndPoint({
            name: 'device',
            path:'/channel/device',
            silent: true
        }),
        new EndPoint({
            name: 'deviceStatus',
            path:'/channel/device/status',
            silent: true
        }),
        new EndPoint({
            name: 'deviceBind',
            path:'/channel/device/bind'
        }),
        new EndPoint({
            name: 'deviceRebind',
            path:'/channel/device/rebind'
        }),
        new EndPoint({
            name: 'deviceUnbind',
            path:'/channel/device/unbind'
        }),
        new EndPoint({
            name: 'helpVideo',
            path:'/channel/help/video'
        })

    ];


    // triggers ==> "login:fix" , if token expired. api will trigger the event.
    window.EZApi = Marionette.Object.extend({
        initialize:function(option){
            option = option || {};
            this.jqueryParameterBase = _.extend({
                cache: false,
                method: 'post',
                dataType: 'jsonp'
            }, option);
            this.urlBase = option.urlBase || "https://channel.iezvu.com/ez";
            this.setToken('');
        },
        _makeParameter:function(){
            var parameter = Object.create(this.jqueryParameterBase);
            return parameter;
        },
        setToken: function(token){
            this._eztoken = token;
        },
        getToken: function(){
            return this._eztoken;
        },
        setAudio: function(audio){
            console.log('to set' + audio);
            window.localStorage.setItem('audio', audio) ;
        },
        getAudio: function(){
            return window.localStorage.getItem('audio') === 'true';
        },
        _makeData:function(data){
            return _.extend({
                token: this.getToken(),
                audio: this.getAudio()
            }, data);
        },
        query: function(url){
            var parameter = this._makeParameter();
            parameter.url = url;
            return $.ajax(parameter);
        }
    });

    $.each(endPoints, function(idx, endPoint){
        window.EZApi.prototype[endPoint.name] = function(data, resource){
            var api = this;
            var parameter = this._makeParameter();
            if(resource){
                parameter.url = this.urlBase + endPoint.path + '/' + resource;
            }
            else{
                parameter.url = this.urlBase + endPoint.path;
            }

            var jqueryData = api._makeData(data);
            _.extend(jqueryData, endPoint.extraData(api));

            parameter.data = jqueryData;
            var query = $.ajax(
                parameter
            );

            var deferred = $.Deferred();
            query.done(function(response){
                if(response.status){
                    console.log(response);
                    endPoint.callBack(api, response);
                    deferred.resolve(response);
                }
                else{
                    window.console.error('statue is false');
                    window.console.error('path is: '+endPoint.path);
                    if(response.error === -1) {
                        if(!endPoint.silent){
                            window.console.error('login:fix triggered');
                            api.trigger("login:fix");
                            api.trigger("authentication:fail", endPoint.path);
                        }
                    }
                    window.console.error(response);
                    deferred.reject(response);
                }
            });

            return deferred.promise();
        };
    });
    EZService.EZApi = window.EZApi;

})(window.Marionette, window.Backbone, window._, window.jQuery);
