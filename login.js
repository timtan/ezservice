(function(Marionette, Backbone, _, $, Views, Models, Service){
    "use strict";

    var EZService = window.EZService || {};
    window.EZService = EZService;

    EZService.LoginService = Service.BaseService.extend({
        initialize: function(option){
            this.facebookHelper = option.facebookHelper;
            this.user = option.user;
            this.api = option.api;
        },
        handleLogin: function(fbdata, type){
            var self = this;
            this.user.set(fbdata);
            var login = self.api.login({
                type: type,
                token: self.user.get("access_token"),
                // if you switch back to facebook login, un comment the following codes
                data: {
                    fbid: self.user.get("user_id"),
                    fbemail: self.user.get("email")
                }
            });
            var deferred = $.Deferred();
            login.done(function(data){
                self.user.setToken(data.token);
                self.user.set({
                    name: data.username
                });
                if(data.service_type){
                    self.user.set({
                        service_type: data.service_type
                    });
                }
                deferred.resolve(data.token);
                self.user.saveToLocalStorage();
            });
            return deferred.promise();
        },
        initialUserData: function(){
            this.user.loadFromLocalStorage();
        },
        doLogin: function(){
            this.facebookHelper.loginAndFetchingData();
        },
        doLogout: function(){
            this.user.cleanData();
            this.user.saveToLocalStorage();
            var $request = this.api.logout();
            var self = this;
            $.when($request).done(function(){
                self.api.setToken("");
                Backbone.history.navigate("/", {trigger:true});
            });
        }
    });


})(window.Marionette, window.Backbone, window._, window.jQuery, window.Views, window.Models, window.Service);
