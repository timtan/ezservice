/* the followings are essential part
 device id (da=...)  done
 checksum  (checksum=...) done
 device display name (ac=...) done
 server location  (u=...) done , it is proxy server here.
 os type (os=...) done
 */
(function (_) {
    'use strict';
    var attributes = ['cid', 'deviceName', 'id', 'checksum', 'server', 'os', 'login_redirect_url'];

    window.URLInfo = {};

    window.URLInfo.loadFromLocalStorage = function(){
        var data = {};
        _.each(attributes, function(key){
            var storedValue = window.localStorage.getItem(key);
            if(storedValue){
                data[key] = storedValue;
            }
        });
        return data;
    };


    window.URLInfo.saveToLocalStorage = function(deviceInfo){
        _.each(deviceInfo, function(value, key){
            window.localStorage.setItem(key, value) ;
        });
    };


    window.URLInfo.parseURLInfo = function() {
        var urlInfo = {};
        var c = 0;
        if ((c = location.href.indexOf('?')) > 0) {
            var raw = location.href.substring(c + 1);
            if ((c = raw.indexOf('#')) > 0)
                raw = raw.substring(0, c);

            var pairs = raw.split('&');
            var device_name = null,
                device_id = null,
                checksum = null,
                proxy_server = null,
                os = null,
                storage = null,
                force_login = null,
                login_redirect_url = null;

            for (var i in pairs) {
                if (0 === pairs[i].indexOf('ac='))
                    device_name = urlInfo.deviceName = pairs[i].substring(3);
                else if (0 === pairs[i].indexOf('si=')) // specified to use cookie or localstorage
                    storage = urlInfo.storage = pairs[i].substring(3);
                else if (0 === pairs[i].indexOf('da='))
                    device_id = urlInfo.id = pairs[i].substring(3);
                else if (0 === pairs[i].indexOf('checksum='))
                    checksum = urlInfo.checksum = pairs[i].substring(9);
                else if (0 === pairs[i].indexOf('u='))
                    proxy_server = urlInfo.server = pairs[i].substring(2);
                else if (0 === pairs[i].indexOf('os='))
                    os = urlInfo.os = pairs[i].substring(3);
                else if (0 === pairs[i].indexOf('login_redirect_url='))
                    login_redirect_url = urlInfo.login_redirect_url = pairs[i].substring(19);
                else if (0 === pairs[i].indexOf('force_login='))
                    force_login = urlInfo.force_login = pairs[i].substring(12);
                //console.log('pair['+pairs[i]+']');
            }
        }
        return urlInfo;
    };
})(window._);