/**
 * Created by tim on 15/4/1.
 */
(function () {
    'use strict';
    window.PlayHook = {};
    window.PlayHook.hookPlayer = function(proxy_server, os){
        if (os) {
            var player_func = null;
            var list_player_func = null;
            if (proxy_server) {    // android, ios, osx
                player_func = function (url, title, thumbnail) {
                    var params = proxy_server + '?action=callback&page=' + encodeURIComponent(url) + '&src=' + encodeURIComponent(url);
                    if (title) {
                        params += '&title=' + encodeURIComponent(title);
                    }
                    if (thumbnail) {
                        params += '&image=' + encodeURIComponent(thumbnail);
                    }
                    try {
                        var xmlHttp = window.XDomainRequest ? new XDomainRequest() : new XMLHttpRequest();
                        xmlHttp.open("GET", params, false);
                        xmlHttp.send(null);
                    } catch (err) {
                    }
                };
                // playlist_json_obj from 'ez/channel/movie/playlist?cid=###'
                list_player_func = function (playlist_json_obj) {
                    //var params = proxy_server + '?action=playlist&data=' + encodeURIComponent(JSON.stringify(playlist_json_obj));
                    var params = proxy_server + '?action=playlist';
                    try {
                        var xmlHttp = window.XDomainRequest ? new XDomainRequest() : new XMLHttpRequest();
                        xmlHttp.open("POST", params, false);
                        //xmlHttp.send(null);
                        xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                        xmlHttp.send("data=" + encodeURIComponent(JSON.stringify(playlist_json_obj)));

                    } catch (err) {
                    }
                };
            }
            document.device_media_player = player_func;
            document.device_media_list_player = list_player_func;
        }
    };
})();