/**
 * Created by tim on 15/4/1.
 */
(function ($) {
    'use strict';
    window.Storage = function(storge){
        this.storage = storge;
        if(!storge){
            this.storage = 'local';
        }
    };

    window.Storage.prototype.setItem = function(key, value){
        if(this.storage === 'local'){
            window.localStorage.setItem(key, value) ;
        }
        else{
            $.cookie(key, value, { expires: 365 });
        }
    };

    window.Storage.prototype.getItem = function(key){
        if(this.storage === 'local'){
           return window.localStorage.getItem(key);
        }
        else{
           return $.cookie(key);
        }
    };

})(window.$);