(function(Marionette, Backbone, _){
    'use strict';

    var EZService = window.EZService || {};
    window.EZService = EZService;


    EZService.User = Backbone.Model.extend({
        defaults:{
            service_type: 'fb',
            access_token: "",
            user_id: "",
            email: "",
            name: "Log In",
            ez_token: "",
            audio: false
        },
        initialize: function(data, api){
            this.on("change:ez_token", function(model, value){
                this.set("is_login", !!value);
            }, this);
            this.api = api;
        },
        checkToken:function(){
            return this.api.devices();  // check done or fail
        },
        saveToLocalStorage: function(){
            _.each(this.attributes, function(value, key){
               window.localStorage.setItem(key, value) ;
            });
        },
        loadFromLocalStorage: function(){
            var self = this;
            _.each(this.defaults, function(value, key){
                var storedValue = window.localStorage.getItem(key);
                if(!storedValue){
                    self.set(key, value);
                }
                else{
                    self.set(key, storedValue);
                }
            });
        },
        isLoggedIn: function(){
           return this.get("is_login");
        },
        getToken: function(){
            return this.get("ez_token");
        },
        setToken: function(token){
            return this.set("ez_token", token);
        },
        cleanData: function(){
           this.set(this.defaults);
        }
    });



})(window.Marionette, window.Backbone, window._);

